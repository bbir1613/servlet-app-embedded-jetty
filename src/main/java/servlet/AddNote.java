package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/addnote")
public class AddNote extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> notes = (List<String>) req.getSession().getAttribute("notes");
        if (notes == null) {
            notes = new ArrayList<>();
        }
        notes.add(req.getParameter("note"));
        req.getSession().setAttribute("notes", notes);
        StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Welcome</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style=\"text-align: center;\">\n" +
                "    <form method = \"POST\" action=\"http://localhost:8080/\">\n" +
                "        <p>Note <input type=\"text\" name=\"note\">\n" +
                "        <p><input type=\"submit\" value=\"Add\">\n" +
                "    </form>\n");
        for (String s : notes) {
            sb.append(s).append("<br>");
        }
        sb.append("</div>\n" +
                "</body>\n" +
                "</html>");
        PrintWriter out = resp.getWriter();
        out.print(sb.toString());
        out.flush();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.print("Hello world");
        out.flush();
    }
}
