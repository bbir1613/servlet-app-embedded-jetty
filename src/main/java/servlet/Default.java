package servlet;

import util.Util;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/default")
public class Default extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> notes = (List<String>) req.getSession().getAttribute("notes");
        if (notes == null) {
            //if there are no notes saved, we return the default page.
            PrintWriter out = resp.getWriter();
            out.print(Util.welcomeHtmlPage);
            out.flush();
            out.close();
        } else {
            //there are notes, so we append them to the document
            StringBuilder sb = new StringBuilder("<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Add note</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"text-align: center;\">\n" +
                    "    <form method = \"POST\" action=\"http://localhost:8080/\">\n" +
                    "        <p>Note <input type=\"text\" name=\"note\">\n" +
                    "        <p><input type=\"submit\" value=\"Add\">\n" +
                    "    </form>\n");
            for (String note : notes) {
                sb.append(note).append("<br>");
            }
            sb.append("</div>\n" +
                    "</body>\n" +
                    "</html>");
            PrintWriter out = resp.getWriter();
            out.print(sb.toString());
            out.flush();
            out.close();
        }
    }
}
