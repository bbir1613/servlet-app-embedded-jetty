package filter;

import util.Util;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter("/")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //get login attribute
        Boolean login = (Boolean) ((HttpServletRequest) request).getSession().getAttribute("login");
        if (login == null || ((HttpServletRequest) request).getMethod().equals("GET")) {
            //if login is null or method is GET we initilize the sesion 
            ((HttpServletRequest) request).getSession(true);
            //set the attribute login to false
            ((HttpServletRequest) request).getSession().setAttribute("login", false);
            PrintWriter out = response.getWriter();
            //send to the client login page.
            out.print(Util.loginHtmlPage);
            out.flush();
            out.close();
        }else{
            //user is login, so we forward the response to the dispatcher
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}